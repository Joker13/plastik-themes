# Plastik theme

## Plastik
![](https://cn.opendesktop.org/img/a/0/d/7/82b9d0574eb075038cf55a2d45dd3d9ae89c.png)

## Plastik Dark
![](https://cn.opendesktop.org/img/3/c/3/2/789446c9b56270411c40e085fa2b57473d94.png)

# Plastik Rosse
![](https://cn.opendesktop.org/img/0/9/b/2/8df00bcb1d5862f4f3003bbacaa65b9c2efc.png)

## Description: 

Simple theme for gnome environment GTK3 and GTK2

It includes theme for Gnome-Shell



## Requirements

### Ubuntu/Mint/Debian distros:

```
sudo apt-get install gtk2-engines-murrine gtk2-engines-pixbuf
```



## Donations 

***Do you like what I do?***

Can you help me through paypal

https://www.paypal.me/giosc